
#ifndef SIEC_SIMULATION_HPP
#define SIEC_SIMULATION_HPP

#include "IReportNotifier.hpp"
#include <cstddef>

void simulate(IReportNotifier report, std::size_t NumberOfRounds){}=0;

#endif //SIEC_SIMULATION_HPP
