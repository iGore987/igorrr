
#ifndef SIEC_PACKAGE_HPP
#define SIEC_PACKAGE_HPP


#include <cstddef>

class Package {
private:
    static std::size_t ElementId;
    const std::size_t _id;
public:
    Package(std::size_t id): _id(id) {}

    const std::size_t& const getid();

};


#endif //SIEC_PACKAGE_HPP
