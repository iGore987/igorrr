

#ifndef SIEC_STOREHOUSE_HPP
#define SIEC_STOREHOUSE_HPP

#include "IPackageReceiver.hpp"
#include "IPackageStocpile.hpp"
#include "ReceiverType.hpp"
#include "Package.hpp"
#include <cstddef>
#include <memory>
#include <map>
#include <optional>

class Storehouse: public IPackageReceiver{
private:
    const std::size_t _id;
    std::unique_ptr<IPackageStocpile> _receiver_ptr;
public:
    Storehouse(std::size_t id, std::unique_ptr<IPackageStocpile> ptr):_id(id){
        this ->_receiver_ptr=ptr;
    }

    void storehouse();

    void show_packages();

    const std::size_t& const getid();

    std::map<ReceiverType,std::size_t> getReceiverid();

    void getPackage(Package* package);
};

#endif //SIEC_STOREHOUSE_HPP
