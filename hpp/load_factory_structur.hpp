

#ifndef SIEC_LOAD_FACTORY_STRUCTUR_HPP
#define SIEC_LOAD_FACTORY_STRUCTUR_HPP

#include <string>
#include <map>

std::map<std::string,std::size_t> load_factory_structure();


#endif //SIEC_LOAD_FACTORY_STRUCTUR_HPP
