
#ifndef SIEC_RAMP_HPP
#define SIEC_RAMP_HPP

#include <cstddef>
#include "Package.hpp"
#include "PackageSender.hpp"
#include "IPackageReceiver.hpp"


class Ramp: public PackageSender, public IPackageReceiver{
private:
    const std::size_t _id;
    const std::size_t _frequency;


public:
    Ramp(std::size_t id, std::size_t frequency, ReceiverPreferences pref):
    _frequency(frequency),
    _id(id),
    PackageSender(pref) {}


    void ramp();

    const std::size_t& const getid();

    Package generate_package();
};


#endif //SIEC_RAMP_HPP
